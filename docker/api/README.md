Sensu API
---------

This requires access to:

 * Rabbit MQ
 * Redis

This provides the REST API to reads events from Redis.

### REST API Configuration

https://docs.sensu.io/sensu-core/1.4/api/configuration/#reference-documentation

### Rabbit MQ Configuration

https://docs.sensu.io/sensu-core/1.4/reference/rabbitmq/#reference-documentation

This is the transport so it must also be configured as such:

https://docs.sensu.io/sensu-core/1.4/reference/transport/#reference-documentation

### Redis Configuration

https://docs.sensu.io/sensu-core/1.4/reference/redis/#redis-definition-specification
