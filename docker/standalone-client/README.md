Sensu Standalone Client
-----------------------

This requires access to:

 * Rabbit MQ

This is a machine that simulates a client.
It will run standalone checks defined in json files and report events.

Standalone checks are checks that are defined on the client using a json file.
The command to run must be present on the client.

This also has a check with a check hook.
To show the result of the check hook, the check will always fail.
The check hook is a command which produces informative output to supplement the check.

### Client Configuration

https://docs.sensu.io/sensu-core/1.4/reference/clients/#client-definition-specification

This also defines a client socket which can be used to push events into:

https://docs.sensu.io/sensu-core/1.4/reference/clients/#client-socket-input

### Rabbit MQ Configuration

https://docs.sensu.io/sensu-core/1.4/reference/rabbitmq/#reference-documentation

This is the transport so it must also be configured as such:

https://docs.sensu.io/sensu-core/1.4/reference/transport/#reference-documentation
