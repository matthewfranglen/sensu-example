Sensu Server
------------

This requires access to:

 * Rabbit MQ
 * Redis
 * Sensu API

This handles:

 * Publishing check requests
 * Monitoring clients
 * Monitoring check results
 * Pruning check results

### Server Configuration

The server can be used to configure:

#### Checks

These are subscription checks that are defined on the server.
The client must have access to the command to run.

https://docs.sensu.io/sensu-core/1.4/reference/checks/#check-definition-specification

#### Handlers

These are actions executed by the server on events.

https://docs.sensu.io/sensu-core/1.4/reference/handlers/#handler-definition-specification

#### Filters

These filter events before they reach handlers.

https://docs.sensu.io/sensu-core/1.4/reference/filters/#filter-definition-specification

#### Mutators

These alter event data and are configured by individual event handlers.

https://docs.sensu.io/sensu-core/1.4/reference/mutators/#mutator-definition-specification

### API Configuration

https://docs.sensu.io/sensu-core/1.4/api/configuration/#api-definition-specification

### Rabbit MQ Configuration

https://docs.sensu.io/sensu-core/1.4/reference/rabbitmq/#reference-documentation

This is the transport so it must also be configured as such:

https://docs.sensu.io/sensu-core/1.4/reference/transport/#reference-documentation

### Redis Configuration

https://docs.sensu.io/sensu-core/1.4/reference/redis/#redis-definition-specification
