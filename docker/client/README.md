Sensu Subscribing Client
------------------------

This requires access to:

 * Rabbit MQ

This is a machine that simulates a client.
It will run checks based on subscriptions and report events.

Subscriptions are checks that are defined on the server.
The command to run must be present on the client.

This also runs an aggregate check.
To make that more realistic the Dockerfile supports the creation of multiple distinct images based on the `${ID}` argument.
Each client must have a unique `name`, the build process changes the `client.json` to add the `${ID}`.

This also subscribes to a check with a check hook.
To show the result of the check hook, the check will always fail.
The check hook is a command which produces informative output to supplement the check.

### Client Configuration

https://docs.sensu.io/sensu-core/1.4/reference/clients/#client-definition-specification

This also defines a client socket which can be used to push events into:

https://docs.sensu.io/sensu-core/1.4/reference/clients/#client-socket-input

### Rabbit MQ Configuration

https://docs.sensu.io/sensu-core/1.4/reference/rabbitmq/#reference-documentation

This is the transport so it must also be configured as such:

https://docs.sensu.io/sensu-core/1.4/reference/transport/#reference-documentation
